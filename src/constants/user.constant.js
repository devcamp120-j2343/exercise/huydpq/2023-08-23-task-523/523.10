export const USER_FETCH_PENDING = "Trang thái đợi khi gọi API"

export const USER_FETCH_SUCCESS = "Trang thái thành công khi gọi API"

export const USER_FETCH_ERROR = "Trang thái lỗi khi gọi API"

export const PAGE_CHANGE = " Sự kiện thay đổi trang"