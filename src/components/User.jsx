import { Button, CircularProgress, Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { fetchUser, pageChangePagination } from "../action/user.action"

const User = () => {
    const dispatch = useDispatch()
    const { users, pending, limit, noPage, currentPage } = useSelector((reduxData) => reduxData.userRouducer)
    
    useEffect(() => {
        dispatch(fetchUser(currentPage, limit))
    }, [currentPage])

    const onBtnChiTiet = (userInfo) => {
        console.log(userInfo)
    }

    const onChangePagibation = (event, value) => {
        dispatch(pageChangePagination(value))
    }

    return (
        <Container>
            <Grid container mt={5}>
                <Grid item sm={12} md={12} lg={12} xs={12}>
                    {
                        pending ?
                            <Grid item sm={12} md={12} lg={12} xs={12}>
                                <CircularProgress />
                            </Grid>
                            :
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>ID</TableCell>
                                            <TableCell align="left">NAME</TableCell>
                                            <TableCell align="left">USERNAME</TableCell>
                                            <TableCell align="left">EMAIL</TableCell>
                                            <TableCell align="left">PHONE</TableCell>
                                            <TableCell align="left">WEBSITE</TableCell>
                                            <TableCell align="center">ACTION</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {users.map((user, index) => (
                                            <TableRow
                                                key={index}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            >
                                                <TableCell component="th" scope="row">{user.id}</TableCell>
                                                <TableCell align="left">{user.name}</TableCell>
                                                <TableCell align="left">{user.username}</TableCell>
                                                <TableCell align="left">{user.email}</TableCell>
                                                <TableCell align="left">{user.phone}</TableCell>
                                                <TableCell align="left">{user.website}</TableCell>
                                                <TableCell align="center"><Button onClick={() => onBtnChiTiet(user)}>Chi tiết</Button></TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                    }
                    <Grid item sm={12} md={12} lg={12} xs={12} mt={5} style={{display: "flex", justifyContent: "center"}}>
                        <Pagination onChange={onChangePagibation} count={noPage} page={currentPage} variant="outlined" />
                    </Grid>

                </Grid>
            </Grid>
        </Container>

    )
}

export default User