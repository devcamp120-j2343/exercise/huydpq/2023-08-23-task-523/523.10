import { PAGE_CHANGE, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constant";

const initialState = {
    users: [],
    pending: false,
    limit: 3,
    noPage: 0,
    currentPage: 1,

}

const userRouducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_FETCH_PENDING:
            state.pending = true
            break;
        case USER_FETCH_SUCCESS:
            state.pending = false
            state.noPage = Math.ceil(action.totalUser / state.limit)
            state.users = action.data
            break;
        case USER_FETCH_ERROR:

            break;
        case PAGE_CHANGE:
            state.currentPage = action.payload
            break;
        default:
            break;
    }
    return { ...state }
}

export default userRouducer