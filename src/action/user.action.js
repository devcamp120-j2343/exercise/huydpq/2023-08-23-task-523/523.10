import { PAGE_CHANGE, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constant"

export const fetchUser = (page, limit) => {
    return async (dispatch) => {
        try {
            var requestoption = {
                method: "GET"
            }

            await dispatch({
                type: USER_FETCH_PENDING
            })

            const reponse = await fetch("https://jsonplaceholder.typicode.com/users", requestoption)
            const dataUser = await reponse.json()

            const param = new URLSearchParams({
                "_start": (page - 1) * limit,
                "_limit": limit
            })

            const reponsePagination = await fetch("https://jsonplaceholder.typicode.com/users?" + param.toString(), requestoption)
            const dataPagination  = await reponsePagination.json()


            return dispatch({
                type: USER_FETCH_SUCCESS,
                totalUser: dataUser.length,
                data: dataPagination
            })
            
        } catch (error) {
            return dispatch({
                type: USER_FETCH_ERROR,
                error: error
            })
        }
    }
}

export const pageChangePagination = (value) => {
    return {
        type: PAGE_CHANGE,
        payload: value
    }
}